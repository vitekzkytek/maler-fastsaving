import requests
import urllib3
import logging
import pandas as pd
from datetime import datetime
from dotenv import load_dotenv
import os
from sqlalchemy import create_engine

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(message)s',
    handlers=[
        logging.FileHandler('./app.log'),
        logging.StreamHandler()
    ]
)


def request_data():
    url = "https://10.200.4.4:8080/cubes/3/analytics/0/sinks/data"

    payload = "{\r\n    \"sequence_number\": \"8\"\r\n}"
    headers = {
    'Accept-Version': '1.3',
    'Content-Type': 'application/json',
    'Authorization': 'Bearer VTLMQILFQG'
    }

    response = requests.request("POST", url, headers=headers, data = payload,verify=False)

    return response


def json_to_frame(data,now):
    l = []
    for sink in data['sinks']:
        for cat in sink['data']['categories']:
            l.append({
                'category':cat['category'],
                'count':cat['count'],
                'name':sink['name'],
                'id':sink['id'],
                'output_value_type':sink['output_value_type'],
                'sequence_number':data['sequence_number'],
                'timestamp':data['timestamp']
            })

    df = pd.DataFrame(l)
    df['created_at'] = now

    return df

def init_db():
    return create_engine(os.getenv('DB_CONNSTRING'))

def store_in_db(db,df):
    try:
        df.to_sql(con=db,schema='vitekzkytek',name='flow_analytics_distribution_viadukt',if_exists='append',index=False)
        logging.info('saved to db ...')
    except Exception as e:
        logging.error('writing to DB failed; Error: {}'.format(str(e)))


def request_transform_save():
    logging.info('Requesting data')

    # request data
    response = request_data()

    if response.status_code == 200:
        logging.info('successful response')
        d = response.json()
        now = datetime.now()

        # transform to dataframe
        df = json_to_frame(d,now)
        logging.info('data parsed, saving to db  ...')

        # save to db
        store_in_db(db,df)
        
    else:
        logging.info('request yielded error: #{} - {}'.format(response.status_code,response.reason))

if __name__ == '__main__':
    urllib3.disable_warnings()

    if os.path.isfile('env.env'):
        load_dotenv('env.env')
        
        if os.getenv('DB_CONNSTRING'):
            db = init_db()

            request_transform_save()        
        else:
            logging.error('DB_CONNSTRING env var not defined')
    else:
        logging.error('Missing env.env file')