# Collecting FLOW viadukt analytics

## install venv

1. create `venv` called `flows`:

`python -m venv flows`

2. install packages from `requirements.txt`:

`.\flows\Scripts\activate`

`pip install -r requirements.txt`


## run 

2. activate venv (from the same directory):

`.\flows\Scripts\activate`

`python -m flow-viadukt-analytics.py`